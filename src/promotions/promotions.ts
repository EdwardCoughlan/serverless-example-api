// we are going to need to setup the database here if we don't already  have it

import { PromotionsController } from './promotions.controller';
import { APIGatewayProxyHandler } from 'aws-lambda';

const controller: PromotionsController = new PromotionsController();
export const addAPromotion: APIGatewayProxyHandler = controller.AddAPromotion;
export const findAPromotion: APIGatewayProxyHandler = controller.findAPromotionById;