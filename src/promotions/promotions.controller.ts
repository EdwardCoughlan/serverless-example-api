import { ApiHandler, ApiResponse, ApiEvent, ApiContext } from "../shared/api.interface";
import { ResponseBuilder } from "../shared/responseBuilder";
import { ErrorCode } from "../shared/errorCodes";
import { IPromotionCreated, IAddAPromotionRequest } from "./promotions.interface";
import { getCollection } from '../services/databaseService';
import { ObjectID } from "bson";
import { IPromotionCreate } from "./promotions.schema";
import { FilterQuery } from "mongodb";



export class PromotionsController {
    readonly _collectionName = 'promotions';
    readonly _databaseName = 'promotions';
    public AddAPromotion: ApiHandler = async (event: ApiEvent, context: ApiContext): Promise<ApiResponse> => {
        try {
            const promotionData: IAddAPromotionRequest = JSON.parse(event.body);

            var collection = await getCollection(this._databaseName, this._collectionName);
            const response = await collection.insertOne({ title: "hi", description: "description" } as IPromotionCreate);
            console.log(response)
            if (response.result.ok) {
                const id = response.insertedId.toHexString();
                return ResponseBuilder.ok<IPromotionCreated>({ id: id, title: promotionData.title });
            } else {
                return ResponseBuilder.unprocessableEntityError(ErrorCode.GeneralError, "Unable to processs request to add promotion");
            }
        } catch (error) {
            console.error('Failed to create promotion: ', error);
            return ResponseBuilder.internalServerError(ErrorCode.GeneralError, error.message);
        }
    }

    public findAPromotionById: ApiHandler = async (event: ApiEvent, context: ApiContext): Promise<ApiResponse> => {
        try {
            const promotionId: string = event.queryStringParameters['id'];
            console.log(promotionId);
            var collection = await getCollection(this._databaseName, this._collectionName);
            var item = await collection.findOne({ _id: new ObjectID(promotionId) });
            console.log(item);
            if (item) {
                return ResponseBuilder.ok(item);
            }
            return ResponseBuilder.ok({});
        } catch (error) {
            console.error('Failed to create promotion: ', error);
            return ResponseBuilder.internalServerError(ErrorCode.GeneralError, error.message);
        }
    }
}
