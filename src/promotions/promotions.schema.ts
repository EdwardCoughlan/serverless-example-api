import { ObjectID } from "bson";

export interface IPromotionCreate {
    title: string,
    description: string
}

export interface IPromotion extends IPromotionCreate {
    id: ObjectID;
}