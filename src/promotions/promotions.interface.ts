export interface IAddAPromotionRequest {
    title: string,
    description: string
}


export interface IPromotionCreated {
    title: string,
    id: string
}