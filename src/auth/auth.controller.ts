
import { CustomAuthorizerEvent, CustomAuthorizerResult, CustomAuthorizerHandler, Context } from 'aws-lambda';


export class AuthController {
    public authenticate: CustomAuthorizerHandler = async (event: CustomAuthorizerEvent, context: Context):
        Promise<CustomAuthorizerResult> => {
        try {
            console.log(event);

            const token: String = event.authorizationToken;

            switch (token) {
                case 'allow':
                    return this.generateAuthPolicy(event, policyEffect.Allow.toString());
                default:
                    return this.generateAuthPolicy(event, policyEffect.Deny.toString());
            }

        } catch (error) {
            console.error('Failed to authorize ', error);
            return this.generateAuthPolicy(event, policyEffect.Deny.toString());
        }
    }

    private generateAuthPolicy(event: CustomAuthorizerEvent, effect: string): CustomAuthorizerResult {
        return {
            "principalId": "user",
            "policyDocument": {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Action": "execute-api:Invoke",
                        "Effect": effect,
                        "Resource": event.methodArn
                    }
                ]
            }
        } as CustomAuthorizerResult;
    }
}

enum policyEffect {
    Allow = "Allow",
    Deny = "Deny"
}