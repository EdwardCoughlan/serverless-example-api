import { AuthController } from './auth.controller';
import { CustomAuthorizerHandler } from 'aws-lambda';

const controller: AuthController = new AuthController();
export const checkAuth: CustomAuthorizerHandler = controller.authenticate;