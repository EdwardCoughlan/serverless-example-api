import { ErrorResponseBody, ApiResponse } from "./api.interface";
import { ErrorResult, InternalServerErrorResult, ForbiddenResult, UnprocessableEntityErrorResult } from "./errors";
import { HttpStatusCodes } from './httpStatusCodes';
import { ErrorCode } from "./errorCodes";

export class ResponseBuilder {
    static unprocessableEntityError(code: ErrorCode, description: string): ApiResponse {
        return ResponseBuilder._buildResponse(new UnprocessableEntityErrorResult(code, description), HttpStatusCodes.UnprocessableEntity)
    }
    public static internalServerError(code: ErrorCode, description: string): ApiResponse {
        return ResponseBuilder._buildResponse(new InternalServerErrorResult(code, description), HttpStatusCodes.InternalServerError)
    }
    public static ok<T>(result: T): ApiResponse {
        return ResponseBuilder._buildResponse(result, HttpStatusCodes.Ok)
    };

    public static forbidden(code: ErrorCode, description: string): ApiResponse {
        return ResponseBuilder._buildResponse(new ForbiddenResult(code, description), HttpStatusCodes.Forbidden)
    }

    private static _buildResponse<T>(result: T, statusCode: number): ApiResponse {
        const bodyObject: ErrorResponseBody | T = result instanceof ErrorResult ?
            { error: result } :
            result;
        return {
            body: JSON.stringify(bodyObject),
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            statusCode
        } as ApiResponse;
    }
}