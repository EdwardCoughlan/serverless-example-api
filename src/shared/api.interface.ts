import { APIGatewayProxyEvent, Context, APIGatewayProxyCallback, APIGatewayProxyResult } from 'aws-lambda';  // tslint:disable-line no-implicit-dependencies (Using only the type information from the @types package.)
import { ErrorResult } from './errors';

// Type aliases to hide the 'aws-lambda' package and have consistent, short naming.
export type ApiCallback = APIGatewayProxyCallback;
export type ApiContext = Context;
export type ApiEvent = APIGatewayProxyEvent;
export type ApiResponse = APIGatewayProxyResult;
export type ApiHandler = (event: ApiEvent, context: ApiContext) => void; // Same as ProxyHandler, but requires callback.

export interface ErrorResponseBody {
    error: ErrorResult;
}
