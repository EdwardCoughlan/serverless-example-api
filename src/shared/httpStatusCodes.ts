export enum HttpStatusCodes {
    Ok = 200,
    InternalServerError = 500,
    Forbidden = 403,
    UnprocessableEntity = 422,
}