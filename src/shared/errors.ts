import { ErrorCode } from "./errorCodes";

// tslint:disable max-classes-per-file (Many simple inherited classes.)

export abstract class ErrorResult extends Error {
    public constructor(public code: ErrorCode, public description: string) {
        super(description);
    }
}

export class BadRequestResult extends ErrorResult { }

export class ConfigurationErrorResult extends ErrorResult { }

export class ForbiddenResult extends ErrorResult { }

export class InternalServerErrorResult extends ErrorResult { }

export class UnprocessableEntityErrorResult extends ErrorResult { }

export class NotFoundResult extends ErrorResult { }

  // tslint:enable
