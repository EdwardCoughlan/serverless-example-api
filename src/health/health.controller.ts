import { ApiHandler, ApiResponse, ApiEvent, ApiContext } from "../shared/api.interface";
import { ResponseBuilder } from "../shared/responseBuilder";
import { GetHealthCheckResult } from './health.interface';
import { ErrorCode } from "../shared/errorCodes";


export class HealthController {
    public getHealthCheck: ApiHandler = async (event: ApiEvent, context: ApiContext): Promise<ApiResponse> => {
        try {
            {
                return ResponseBuilder.ok({ success: true, event: event, context: context } as GetHealthCheckResult);
            };
        } catch (error) {
            console.error('Not so healthy: ', error);
            return ResponseBuilder.internalServerError(ErrorCode.GeneralError, error.message);
        }
    }
}
