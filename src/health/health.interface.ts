export interface GetHealthCheckResult {
    success: boolean;
    context: any;
    event: any;
}