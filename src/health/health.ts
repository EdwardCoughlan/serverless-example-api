import { HealthController } from './health.controller';
import { APIGatewayProxyHandler } from 'aws-lambda';

const controller: HealthController = new HealthController();
export const getHealthCheck: APIGatewayProxyHandler = controller.getHealthCheck;
export const getHealthCheckWithAuth: APIGatewayProxyHandler = controller.getHealthCheck;