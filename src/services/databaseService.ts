import { MongoClient } from 'mongodb'

export const connectToMongoDb = async () => {
    let mongoClient: MongoClient = null
    mongoClient = await MongoClient.connect(process.env.DB)
    return mongoClient;
}

export const getCollection = async (databaseName: string, collectionName: string) => {
    const client = await connectToMongoDb();
    const database = client.db(databaseName);
    if (database) {
        const collection = database.collection(collectionName);
        if (collection) {
            return collection;
        } else {
            throw new Error(`Unable to find collection ${collectionName}`);
        }
    } else {
        throw new Error(`Unable to find database ${databaseName}`);
    }
}